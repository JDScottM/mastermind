var randomNumber1; // var means "variable"
var randomNumber2;
var randomNumber3;
var randomNumber4;
var currentRow;

var guess1 = 0;
var guess2 = 0;
var guess3 = 0;
var guess4 = 0;

var guessesArr = [0, 0, 0, 0];

var howManyGuesses = 0;

var guesses = "Your guesses are: ";

const blue = 1;
const red = 2;
const green = 3;
const silver = 4;
const yellow = 5;
const white = 6;
const pink = 7;
const orange = 8;

/*
 * allows a player to guess the number
 * it cheeks if they got the right number 
 * it tells them is they are right or worng
 */
var guessButtonClicked = function () {
    var theirGuess = $("#guessField").val();

    guesses = guesses + theirGuess + " ";

    $("#guessesLabel").text(guesses);

    console.log(theirGuess);
    var randomNumber1String = randomNumber1.toString();
    if (theirGuess === randomNumber1String) {
        // This is to tell them they're right and disable the fields

        console.log("RIGHTT!");
        $("#resultLabel").text("RIGHTT " + randomNumber1);

        $("#buttonGuess").prop('disabled', true);

        $("#guessField").prop('disabled', true);

    } else {
        // This is executed if they are wrong

        console.log("wrong");
    }
    $("#resultdiv").show();

    $("#guessField").val("");


};

/*
 * This is what is called when the play button is clicked
 * It shows the box to make guesses
 * and makes a random number for the player to guess
 */
var playButtonClicked = function () {
    randomNumber1 = Math.floor((Math.random() * 8) + 1);

    console.log(randomNumber1);
    //$("#randomNumber").text("our first number is " + randomNumber1);
    $("#geeesissDiv").show();
    $("#buttonPlay").prop('disabled', true);
    $("#buttoncPlay").prop('disabled', true);
};

/*
 * This is what is called when the play with colours button is clicked
 * It shows the box to make guesses
 * and makes a random number for the player to guess
 */
var playcButtonClicked = function () {
    randomNumber1 = Math.floor((Math.random() * 8) + 1);
    randomNumber2 = Math.floor((Math.random() * 8) + 1);
    randomNumber3 = Math.floor((Math.random() * 8) + 1);
    randomNumber4 = Math.floor((Math.random() * 8) + 1);


    console.log(randomNumber1);
    //$("#randomNumber").text("our first number is " + randomNumber1);
    $("#guesColourdiv").show();
    $("#buttonPlay").prop('disabled', true);
    $("#buttoncPlay").prop('disabled', true);
};

var colourButtonClicked = function (colour) {
    $("#resultsColourdiv").show();

    var tableData = "";
    var isFirstGuessOnRow = false;

    if (guessesArr[0] === 0) {
        guessesArr[0] = colour;
        howManyGuesses = 1;
        $("#guessesTable").append("<tr>");
        isFirstGuessOnRow = true;
    }
    else if (guessesArr[1] === 0) {
        guessesArr[1] = colour;
        howManyGuesses = 2;
    }
    else if (guessesArr[2] === 0) {
        guessesArr[2] = colour;
        howManyGuesses = 3;
    }
    else if (guessesArr[3] === 0) {
        guessesArr[3] = colour;
        howManyGuesses = 4;
        $("#buttonBlue").prop('disabled', true);
        $("#buttonRed").prop('disabled', true);
        $("#buttonGreen").prop('disabled', true);
        $("#buttonSilver").prop('disabled', true);
        $("#buttonYellow").prop('disabled', true);
        $("#buttonWhite").prop('disabled', true);
        $("#buttonPink").prop('disabled', true);
        $("#buttonOrange").prop('disabled', true);


    }

    if (colour === blue) {
        tableData = '<td class="blue guess"></td>'
    }
    if (colour === red) {
        tableData = '<td class="red guess"></td>'
    }
    if (colour === green) {
        tableData = '<td class="green guess"></td>'
    }
    if (colour === silver) {
        tableData = '<td class="grey guess"></td>'
    }
    if (colour === yellow) {
        tableData = '<td class="yellow guess"></td>'
    }
    if (colour === white) {
        tableData = '<td class="white guess"></td>'
    }
    if (colour === pink) {
        tableData = '<td class="pink guess"></td>'
    }
    if (colour === orange) {
        tableData = '<td class="orange guess"></td>'
    }


    $("#guessesTable tr:last").append(tableData);

};

var checkRightPlaceNumbers = function () {
    var numberRight = 0;

    if (randomNumber1 === guessesArr[0]) {
        numberRight = numberRight + 1;
    }
    if (randomNumber2 === guessesArr[1]) {
        numberRight = numberRight + 1;
    }
    if (randomNumber3 === guessesArr[2]) {
        numberRight = numberRight + 1;
    }
    if (randomNumber4 === guessesArr[3]) {
        numberRight = numberRight + 1;
    }
    return numberRight + " in the right place";
}
var checkRightColourNumbers = function () {
    var rightColour = 0;

    // guessesArr[0]
    if (randomNumber1 != guessesArr[0]) {
        if (randomNumber2 === guessesArr[0]) {
            rightColour = rightColour + 1;
        } else {
            if (randomNumber3 === guessesArr[0]) {
                rightColour = rightColour + 1;
            } else {
                if (randomNumber4 === guessesArr[0]) {
                    rightColour = rightColour + 1;
                }
            }

        }
    }
    // guessesArr[1]
    if (randomNumber2 != guessesArr[1]) {
        if (randomNumber1 === guessesArr[1]) {
            rightColour = rightColour + 1;
        } else {
            if (randomNumber3 === guessesArr[1]) {
                rightColour = rightColour + 1;
            } else {
                if (randomNumber4 === guessesArr[1]) {
                    rightColour = rightColour + 1;
                }
            }

        }
    }
    // guessesArr[2]
    if (randomNumber3 != guessesArr[2]) {
        if (randomNumber1 === guessesArr[2]) {
            rightColour = rightColour + 1;
        } else {
            if (randomNumber2 === guessesArr[2]) {
                rightColour = rightColour + 1;
            } else {
                if (randomNumber4 === guessesArr[2]) {
                    rightColour = rightColour + 1;
                }
            }

        }
    }
    // guessesArr[3]
    if (randomNumber4 != guessesArr[3]) {
        if (randomNumber1 === guessesArr[3]) {
            rightColour = rightColour + 1;
        } else {
            if (randomNumber2 === guessesArr[3]) {
                rightColour = rightColour + 1;
            } else {
                if (randomNumber3 === guessesArr[3]) {
                    rightColour = rightColour + 1;
                }
            }

        }
    }

    return rightColour + " in the wrong place";
    }

    var guessColourClicked = function () {
        //if (colour === randomNumber1) {
        //    result = "RIGHT!";

        //    $("#buttonBlue").prop('disabled', true);
        //    $("#buttonRed").prop('disabled', true);
        //    $("#buttonGreen").prop('disabled', true);
        //    $("#buttonSilver").prop('disabled', true);
        //    $("#buttonYellow").prop('disabled', true);
        //    $("#buttonWhite").prop('disabled', true);
        //    $("#buttonPink").prop('disabled', true);
        //    $("#buttonOrange").prop('disabled', true);
        //}
        console.log("r1:" + randomNumber1 + " r2:" + randomNumber2 + " r3:" + randomNumber3 + "r4:" + randomNumber4);

        var resultRightPlace = checkRightPlaceNumbers();
        var resultRightPlaceCell = '<td>' + resultRightPlace + '</td>';
        $("#guessesTable tr:last").append(resultRightPlaceCell);

        var resultRightColourNumbers = checkRightColourNumbers();
        var resultRightColourCell = '<td>' + resultRightColourNumbers + '</td>';
        $("#guessesTable tr:last").append(resultRightColourCell);


        guessesArr = [0, 0, 0, 0];

        howManyGuesses = 0;
        $("#buttonBlue").prop('disabled', false);
        $("#buttonRed").prop('disabled', false);
        $("#buttonGreen").prop('disabled', false);
        $("#buttonSilver").prop('disabled', false);
        $("#buttonYellow").prop('disabled', false);
        $("#buttonWhite").prop('disabled', false);
        $("#buttonPink").prop('disabled', false);
        $("#buttonOrange").prop('disabled', false);



    }

    var backButtonClicked = function () {
        var itemToDel = howManyGuesses - 1;
        guessesArr[itemToDel] = 0;
        howManyGuesses = howManyGuesses - 1;
        $("#guessesTable td:last").remove();
        $("#buttonBlue").prop('disabled', false);
        $("#buttonRed").prop('disabled', false);
        $("#buttonGreen").prop('disabled', false);
        $("#buttonSilver").prop('disabled', false);
        $("#buttonYellow").prop('disabled', false);
        $("#buttonWhite").prop('disabled', false);
        $("#buttonPink").prop('disabled', false);
        $("#buttonOrange").prop('disabled', false);
    }
